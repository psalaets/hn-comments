---
layout: layout.njk
---

# {{title}}

This bookmarklet searches Hacker News for comments about the current page.

## Install

Drag this link to the bookmarks toolbar: <a href="javascript:{{code}}">{{title}}</a>

## Usage

1. Open any webpage
2. Click your {{title}} bookmark
