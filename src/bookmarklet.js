/**
 * @typedef {Object} HnSearchResult
 * @property {string} title
 * @property {string} url
 * @property {string} created_at ISO-8601 formatted date string
 * @property {string} author
 * @property {number} points Upvotes
 * @property {number} num_comments
 * @property {string} objectID `https://news.ycombinator.com/item?id=<objectID>`
 *
 * @typedef {Object} HnSearchResults
 * @property {Array<HnSearchResult>} hits
 */

const document = window.document
const modalId = 'hn-comments-modal';

/**
 *
 * @param {() => void} onClose
 */
function createDialog(onClose) {
  const dialog = document.createElement('dialog');
  dialog.id = modalId;
  dialog.style.cssText = `all: revert; font-family: Verdana, Geneva, sans-serif; padding: 0; color: #111; background-color: #fff; border: 0; border-radius: 5px;`;

  const contentId = 'hn-comments-content';
  const closeButtonId = 'hn-comments-close-button';

  dialog.innerHTML = `
<style>
#${modalId} a:link {color: #1f75cb !important}
#${modalId} a:visited {color: #800080 !important}
#${modalId}::backdrop {background-color: rgba(0,0,0,0.5) !important}
</style>
<div style="padding: 1.2rem; max-height: 80vh; overflow-y: auto;" id="hn-comments-container">
<div id="${contentId}" aria-live="polite"></div>
<div style="margin-top: 1rem; display: flex; justify-content: center;">
<button id="${closeButtonId}" style="all: revert; color: #111; background-color: lightgray; font-size: 1em; padding: .5em 1em">Close</button>
</div>
</div>
`;

  document.body.appendChild(dialog);

  const closeButton = document.getElementById(closeButtonId);

  // Close modal when close button clicked
  closeButton.addEventListener('click', () => dialog.close());

  // Close modal when backdrop clicked
  dialog.addEventListener('click', event => {
    // Works because dialog has no padding
    if (event.target === dialog) {
      dialog.close();
    }
  });

  // Clean up when closing modal
  dialog.addEventListener('close', () => {
    onClose();
    dialog.remove();
  });

  return {
    dialog,
    closeButton,
    content: document.getElementById(contentId),
  };
}

if (!document.getElementById(modalId)) {
  /** For canceling search request when modal closes */
  const abortController = new AbortController();
  const { dialog, closeButton, content } = createDialog(() => abortController.abort());
  const url = currentUrl();

  renderBusy(content, closeButton);
  dialog.showModal();

  search(url, abortController.signal)
    .then(results => renderResults(content, closeButton, results))
    .catch(error => {
      console.error(error);

      if (abortController.signal.aborted) {
        dialog.close();
      } else {
        renderError(content, closeButton, error, url);
      }
    });
}

function currentUrl() {
  const url = new URL(window.location);
  // trim off stuff that probably won't be relevant in search
  url.search = '';
  url.hash = '';
  return url.toString();
}

/**
 * @param {HTMLElement} content
 * @param {HTMLButtonElement} closeButton
 */
function renderBusy(content, closeButton) {
  closeButton.textContent = 'Cancel';

  content.setAttribute('aria-busy', 'true');
  content.innerHTML = `<div>Checking HN...</div>`;
}

/**
 * @param {HTMLElement} content
 * @param {HTMLButtonElement} closeButton
 */
function renderResults(content, closeButton, results) {
  closeButton.textContent = 'Close';

  content.setAttribute('aria-busy', 'false');

  const hitsWithComments = results.hits.filter(hit => hit.num_comments);
  if (hitsWithComments.length > 0) {
    const countDigits = (num) => `${num}`.length;
    const maxCommentCountDigits = Math.max(...hitsWithComments.map(hit => countDigits(hit.num_comments)));
    const cellStyles = 'padding: .4rem .8rem; background-color: #fff; border: 1px solid #f2f2f2;';
    const tableStyles = 'all: revert; border-collapse: collapse;';

    content.innerHTML = `<table style="${tableStyles}"><tr><th style="${cellStyles}">Date</th><th style="${cellStyles}">Comments</th></tr>
${
  hitsWithComments.map(hit => {
    const paddingNeeded = maxCommentCountDigits - countDigits(hit.num_comments);
    const padding = paddingNeeded > 0
      ? `<span style="visibility: hidden;">${'0'.repeat(paddingNeeded)}</span>` : '';
    return `<tr>
<td style="${cellStyles}">${hit.created_at.slice(0, 10)}</td>
<td style="${cellStyles}">${padding}<a href="https://news.ycombinator.com/item?id=${hit.objectID}" target="_blank">${hit.num_comments} comment${hit.num_comments == 1 ? '' : 's'}</a></td>
</tr>`
  }).join('')
}
</table>`;
  } else {
    content.innerHTML = '<output>No comments found</output>';
  }
}

function renderError(content, closeButton, error, url) {
  closeButton.textContent = 'Close';

  content.setAttribute('aria-busy', 'false');
  content.innerHTML = `Failure: ${escapeForHtml(error.message)}<br>
Try the <a href="https://hn.algolia.com/?q=${encodeURIComponent(url)}" target="_blank">Hacker News search form</a>`;
}

function escapeForHtml(str) {
  return str
    .replaceAll('&', '&amp;')
    .replaceAll('<', '&lt;')
    .replaceAll('>', '&gt;')
    .replaceAll('"', '&quot;')
    .replaceAll("'", '&#039;');
}

/**
 * @param {string} url
 * @param {AbortSignal} signal For canceling the request
 * @returns {Promise<HnSearchResults>}
 */
function search(url, signal) {
  const queryParams = new URLSearchParams({
    query: url,
    restrictSearchableAttributes: 'url'
  });

  return fetch(`https://hn.algolia.com/api/v1/search_by_date?${queryParams}`, {
    signal
  })
    .then(resp =>
      resp.ok
        ? resp.json()
        : Promise.reject(new Error(`${resp.status} from HN search API`))
    );
}
